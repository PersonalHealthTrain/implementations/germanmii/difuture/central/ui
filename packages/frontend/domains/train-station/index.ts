export const TrainStationStatusOpen = 'open';
export const TrainStationStatusApproved = 'approved';
export const TrainStationStatusRejected = 'rejected';

export const TrainStationStatusOptions = {
    TrainStationStatusOpen,
    TrainStationStatusApproved,
    TrainStationStatusRejected
}
