export const TrainResultStateOpen = 'open';
export const TrainResultStateDownloading = 'downloading';
export const TrainResultStateDownloaded = 'downloaded';
export const TrainResultStateExtracting = 'extracting';
export const TrainResultStateExtracted = 'extracted';
export const TrainResultStateFinished = 'finished';
export const TrainResultStateFailed = 'failed';
